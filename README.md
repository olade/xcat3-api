# XCAT3 API

A standalone Java project for the API of XCAT3 (alpha), a component-based framework for writing OGSI-compatible CCA
components. This is not my library, I simply packaged it for easier access.

See the [XCAT Project](http://www.extreme.indiana.edu/xcat/) and specifically the 
[XCAT3 Tutorial](http://www.extreme.indiana.edu/xcat/tutorial/xcat3.html).

> XCAT is the Indiana University, [Extreme Lab](http://www.extreme.indiana.edu/) implementation of the 
> [Common Component Architecture](http://www.cca-forum.org/) (CCA).

