/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.10 $ $Author: srikrish $ $Date: 2004/09/07 00:05:53 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.ccacore;

import gov.cca.Services;

/**
 * Extension of the gov.cca.Services interface to add 
 * additional methods needed by XCAT
 */
public interface XCATServices extends Services, XCATCompositionUtil {

  /**
   * Sets the TypeMap properties for the component
   * @param properties the TypeMap properties object for the component
   */
  public void setProperties(gov.cca.TypeMap properties)
    throws gov.cca.CCAException;

  /**
   * Gets the TypeMap properties for the component
   */
  public gov.cca.TypeMap getProperties()
    throws gov.cca.CCAException;

  /**
   * Register a request for a Port that will be retrieved subsequently 
   * with a call to getPort().
   * @param portName A string uniquely describing this port.  This string
   * must be unique for this component, over WS, uses and provides ports.
   * @param type A string desribing the type of this port.
   * @param properties A TypeMap describing properties
   * associated with this port. The property "portClass" which specifies
   * the fully qualified class name of the interface is mandatory
   */
  public void registerWSPort(java.lang.String portName,
			     java.lang.String type,
			     gov.cca.TypeMap properties) 
    throws gov.cca.CCAException;

  /**
   * Notify the framework that a Port, previously registered by this
   * component but currently not in use, is no longer desired. 
   * @param portName The name of a registered Port.
   */
  public void unregisterWSPort(java.lang.String portName) 
    throws gov.cca.CCAException;


  /**
   * Fetch a remote reference for a previously register port (defined by either 
   * addProvidesPort or (more typically) registerUsesPort, registerWSPort).  
   * @param portName The previously registered uses/provides/WS port which
   * 	   the component now wants to use.
   */
  public soaprmi.Remote getRemoteRef(String portName) 
    throws gov.cca.CCAException;

  /**
   * Release a remote reference being held by a getPort/getRemoteRef invocation
   * @param portName The previously fetched remote reference that needs to be
   *                 released
   */
  public void releaseRemoteRef(String portName)
    throws gov.cca.CCAException;
}
