/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.7 $ $Author: srikrish $ $Date: 2004/09/07 20:00:38 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.ccacore;

/**
 * The common methods that are implemented by the XCATServices and XCATComponentID,
 * that are added to aid component composition
 */
public interface XCATCompositionUtil {

  /**
   * Returns an array of registered provides ports
   */
  public String[] getProvidedPortNames() 
    throws gov.cca.CCAException;
  
  /**
   * Returns an array of registered uses ports
   */
  public String[] getUsedPortNames()
    throws gov.cca.CCAException;

  /**
   * Returns the type of the port specified
   * @param providesPortName the name of the provides port whose type is desired
   */
  public String getProvidesPortType(String providesPortName) 
    throws gov.cca.CCAException;
  
  /**
   * Returns the type of the port specified
   * @param usesPortName the name of the uses port whose type is desired
   */
  public String getUsesPortType(String usesPortName)
    throws gov.cca.CCAException;

  /**
   * Returns the GSH for the specified provides port
   * @param providesPortName the name by which the Provides Port is registered
   */
  public String getPortHandle(String providesPortName)
    throws gov.cca.CCAException;

  /**
   * Returns a GSH for the requested portName, and increments the number of users
   * for this port
   * @param providesPortName the name by which the Provides Port is registered
   */
  public String incrementUsers(String providesPortName)
    throws gov.cca.CCAException;

  /**
   * Sets the Provides Port GSH for a uses port during a connect call
   * @param providerComponentName the instanceName of the component providing the port
   * @param providerComponentHandle the GSH for the providing component
   * @param userComponentName the instanceName of the component using the port
   * @param userComponentHandle the GSH for the using component
   * @param providingPortName the name by which the Provides Port is registered
   * @param usingPortName the name by which the Uses Port is registered
   * @param providesPortHandle GSH for the Provides Port after the connect
   */
  public void addUsesConnection(String providerComponentName,
				String providerComponentHandle,
				String userComponentName,
				String userComponentHandle,
				String providingPortName,
				String usingPortName,
				String providesPortHandle)
    throws gov.cca.CCAException;

  /**
   * Disconnects the uses side from a connection
   * @param usingPortName the name of the uses port which is connected
   */
  public void disconnectProvider(String usingPortName)
    throws gov.cca.CCAException;

  /**
   * Disconnects the provides side from a connection
   * @param providingPortName the name of the provides port which is connected
   */
  public void disconnectUser(String providingPortName)
    throws gov.cca.CCAException;

  /**
   * Returns the connection information for this uses port
   * @param usingPortName the name of the uses port which is connected
   */
  public XCATConnectionInfo getConnectionInfo(String usingPortName)
    throws gov.cca.CCAException;

  /**
   * Sets the connection information for a Web service connection
   * @param wsPortName the name that a WS port has been registered as
   * @param endPointLocation the URL for the Web service to connect to
   */
  public void addWSConnection(String wsPortName,
			      String endPointLocation)
    throws gov.cca.CCAException;

  /**
   * Disconnects the Web services connection
   * @param wsPortName the name that a WS port has been registered as
   */
  public void disconnectWS(String wsPortName)
    throws gov.cca.CCAException;
}
