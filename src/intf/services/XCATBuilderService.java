/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.10 $ $Author: srikrish $ $Date: 2004/09/07 23:20:53 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.services;

import intf.ports.XCATPort;
import intf.ccacore.XCATComponentID;
import gov.cca.ports.BuilderService;
import soaprmi.ogsi.xsoap_interface.XSoapHandleResolverInterface;

/**
 * XCAT's internal BuilderService interface that also extends an XCATPort
 * and the GSX Handle Resolver
 */
public interface XCATBuilderService extends BuilderService, 
					    XCATPort, 
					    XSoapHandleResolverInterface {

  // the default timeout for component creation: 30s
  public static final long DEFAULT_TIMEOUT = 30000;

  /**
   * The remote instantiator uses this method on the Builder Service to
   * notify successful creation of a component
   * 
   * @param instanceName the name of the component instance
   * @param componentIDHandle the GSH of the remote component
   */
  public void instantiationComplete(String instanceName,
				    String componentIDHandle)
    throws gov.cca.CCAException;

  /**
   * Connect a WS port of a component to a Web service endpoint
   * @param user the ComponentID for the component that is to be connected
   * @param wsPortName the name of the port that is to be connected
   * @param endPointLocation the URL for the Web service endpoint
   */
  public void connectToWS(XCATComponentID user,
			  String wsPortName,
			  String endPointLocation)
    throws gov.cca.CCAException;

  /**
   * Disconnect a WS port of a component that is connected to a Web service
   * @param user the ComponentID for the component that is to be connected
   * @param wsPortName the name of the port that is to be connected
   */
  public void disconnectFromWS(XCATComponentID user,
			       String wsPortName)
    throws gov.cca.CCAException;
}
