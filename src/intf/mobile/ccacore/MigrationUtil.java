/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.7 $ $Author: srikrish $ $Date: 2004/03/24 00:50:07 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.mobile.ccacore;

/**
 * The common methods that are implemented by the MobileServices and MobileComponentID
 * to support component migration
 */
public interface MigrationUtil {

  //------------------------------------------------------------//
  //    List of methods to support migration on the uses side   //
  //------------------------------------------------------------//

  /**
   * An operation to notify the uses side that the provider component
   * requests migration
   * @param providesComponentName the name of the provides side requesting migration
   * @param usingPortName the name of the uses port that is connected to
   *                      the component requesting migration
   * @param coordinatorHandle the GSH for the coordinator which is needed for
   *                          a callback when the uses side has released the port
   */
  public void requestMigration(String providesComponentName,
			       String usingPortName,
			       String coordinatorHandle)
    throws gov.cca.CCAException;

  /**
   * A notification that the component connected to this via the uses port is 
   * currently in migration
   * @param usingPortName the name of the uses port that is connected to
   *                      the component undergoing migration
   */
  public void confirmMigration(String usingPortName)
    throws gov.cca.CCAException;

  /**
   * A notification that the component connected to this via the uses port 
   * has completed its migration and is ready for use
   * @param usingPortName the name of the uses port that is connected to
   *                      the component undergoing migration
   */
  public void migrationComplete(String usingPortName)
    throws gov.cca.CCAException;

  //-------------------------------------------------------------//
  //  List of methods to support migration on the provides side  //
  //-------------------------------------------------------------//

  /**
   * A request from the Application Coordinator to freeze THIS component.
   * Sends a notification to the coordinator after all outgoing calls are complete,
   * and the component is frozen.
   * @param coordinatorHandle the GSH for the coordinator to callback after
   *                          the component is frozen
   */
  public void freezeComponent(String coordinatorHandle)
    throws gov.cca.CCAException;

  /**
   * Resumes execution of a component that has been frozen
   */
  public void unfreezeComponent()
    throws gov.cca.CCAException;
}
