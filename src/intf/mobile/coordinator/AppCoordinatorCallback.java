/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.9 $ $Author: srikrish $ $Date: 2004/09/09 06:55:24 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.mobile.coordinator;

import intf.ports.XCATPort;

/**
 * The AppCoordinatorCallback interface that is used by the components
 * to send back notifications about checkpoint/migration status
 */

public interface AppCoordinatorCallback extends XCATPort {

  // status returned by the remote side
  public final int WAITING = 0;
  public final int READY = 1;
  public final int FROZEN = 2;
  public final int CHECKPOINTED = 3;
  public final int EXCEPTION = 4; // in the future, this will be specialized

  //----------------------------------------------------//
  //   List of callback methods for the AppCoordinator  //
  //----------------------------------------------------//

  /**
   * A notification that the component using this uses port is 
   * ready for migration of the provides side or not
   * @param providesComponentName the name of the provides side requesting migration
   * @param componentName the instance name for component with the uses port
   * @param usingPortName the name of the uses port that is connected to
   *                      the component undergoing migration
   * @param status        Possible values: AppCoordinatorCallback.READY, 
   *                                       AppCoordinatorCallback.EXCEPTION
   */
  public void migrationApproval(String providesComponentName,
				String componentName, 
				String usingPortName,
				int status)
    throws gov.cca.CCAException;

  /**
   * A notification that this component is frozen
   * @param componentName the instance name of the component
   * @param status        Possible values: AppCoordinatorCallback.FROZEN, 
   *                                       AppCoordinatorCallback.EXCEPTION
   */
  public void componentFrozen(String componentName,
			      int status)
    throws gov.cca.CCAException;

  /**
   * A notification that the component has stored its state
   * @param componentName the instance name of the component undergoing migration
   * @param individualStorageServiceURL the URL of the Individual Storage Service used
   * @param storageID the storageID returned by the Individual Storage Service
   * @param status        Possible values: AppCoordinatorCallback.FROZEN, 
   *                                       AppCoordinatorCallback.EXCEPTION
   */
  public void componentCheckpointed(String componentName,
				    String individualStorageServiceURL,
				    String storageID,
				    int status)
    throws gov.cca.CCAException;
}
