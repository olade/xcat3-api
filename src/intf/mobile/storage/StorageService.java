/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.4 $ $Author: srikrish $ $Date: 2004/04/30 05:31:49 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

package intf.mobile.storage;

import intf.ports.XCATPort;

/**
 * Interface for the service that stores component state
 */

public interface StorageService extends XCATPort {

  //---------------------------------------------------------------//
  // Methods used during migration to store individual checkpoints //
  //---------------------------------------------------------------//
  
  /**
   * Stores the individual state of the component during migration
   * @param componentHandle the GSH for the component
   * @param componentState the stringified (XML) state of component
   */
  public void putIndividualComponentState(String componentHandle,
					  String componentState) 
    throws gov.cca.CCAException;

  /**
   * Returns the state of the specified single component during migration
   * @param componentHandle the GSH for the component
   */
  public String getIndividualComponentState(String componentHandle)
    throws gov.cca.CCAException;  

  //-------------------------------------------------------------------------//
  // Methods used during distributed checkpointing to store global snapshots //
  //-------------------------------------------------------------------------//

  /**
   * Initializes the distributed checkpoint, in anticipation of a new one
   * @param applicationID the unique ID for the application (set of components)
   */
  public void initializeCheckpoint(String applicationID)
    throws gov.cca.CCAException;

  /**
   * Adds the state of this component to the current checkpoint
   * @param applicationID the unique ID for the application (set of components)
   * @param componentHandle the GSH for the component
   * @param componentState the stringified (XML) state of component
   */
  public void appendToCheckpoint(String applicationID,
				 String componentHandle,
				 String componentState)
    throws gov.cca.CCAException;

  /**
   * Notification that this checkpoint is complete and can be atomically
   * committed to stable storage
   * @param applicationID the unique ID for the application (set of components)
   */
  public void commitCheckpoint(String applicationID)
    throws gov.cca.CCAException;

  /**
   * Gets the state of a component from a distributed checkpoint
   * @param applicationID the unique ID for the application (set of components)
   * @param componentHandle the GSH for the component
   */
  public String getComponentStateFromCheckpoint(String applicationID,
						String componentHandle)
    throws gov.cca.CCAException;
}
