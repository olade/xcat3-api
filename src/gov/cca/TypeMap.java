/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.4 $ $Author: srikrish $ $Date: 2004/03/01 20:31:41 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * File:          TypeMap.java
 * Symbol:        gov.cca.TypeMap-v0.6
 * Symbol Type:   interface
 * Babel Version: 0.8.6
 * SIDL Created:  20031103 16:08:38 EST
 * Generated:     20031103 16:08:44 EST
 * Description:   Client-side glue code for gov.cca.TypeMap
 * 
 */

package gov.cca;

/**
 * Symbol "gov.cca.TypeMap" (version 0.6)
 * 
 *  A CCA map.  Maps a string key to a particular value. Types are
 *  strictly enforced.  For example, values places into the map
 *  using putInt can be retrieved only using getInt.  Calls to
 *  getLong, getString, getIntArray and other get methods will
 *  fail (i.e. return the default value). 
 */
public interface TypeMap {

  /**
   * Create an exact copy of this Map 
   */
  public abstract gov.cca.TypeMap cloneTypeMap();

  /**
   * Create a new Map with no key/value associations. 
   */
  public abstract gov.cca.TypeMap cloneEmpty();

  /**
   * Method:  getInt[]
   */
  public abstract int getInt(java.lang.String key,
			     int dflt) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  getLong[]
   */
  public abstract long getLong(java.lang.String key,
			       long dflt) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  getFloat[]
   */
  public abstract float getFloat(java.lang.String key,
				 float dflt) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  getDouble[]
   */
  public abstract double getDouble(java.lang.String key,
				   double dflt) 
    throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getFcomplex[]
//     */
//    public abstract SIDL.FloatComplex getFcomplex(java.lang.String key,
//  						SIDL.FloatComplex dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getDcomplex[]
//     */
//    public abstract SIDL.DoubleComplex getDcomplex(java.lang.String key,
//  						 SIDL.DoubleComplex dflt) 
//      throws gov.cca.TypeMismatchException;

  /**
   * Method:  getString[]
   */
  public abstract java.lang.String getString(java.lang.String key,
					     java.lang.String dflt) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  getBool[]
   */
  public abstract boolean getBool(java.lang.String key,
				  boolean dflt) 
    throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getIntArray[]
//     */
//    public abstract SIDL.Integer.Array1 getIntArray(java.lang.String key,
//  						  SIDL.Integer.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getLongArray[]
//     */
//    public abstract SIDL.Long.Array1 getLongArray(java.lang.String key,
//  						SIDL.Long.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getFloatArray[]
//     */
//    public abstract SIDL.Float.Array1 getFloatArray(java.lang.String key,
//  						  SIDL.Float.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getDoubleArray[]
//     */
//    public abstract SIDL.Double.Array1 getDoubleArray(java.lang.String key,
//  						    SIDL.Double.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getFcomplexArray[]
//     */
//    public abstract SIDL.FloatComplex.Array1 getFcomplexArray(java.lang.String key,
//  							    SIDL.FloatComplex.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getDcomplexArray[]
//     */
//    public abstract SIDL.DoubleComplex.Array1 getDcomplexArray(java.lang.String key,
//  							     SIDL.DoubleComplex.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getStringArray[]
//     */
//    public abstract SIDL.String.Array1 getStringArray(java.lang.String key,
//  						    SIDL.String.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  getBoolArray[]
//     */
//    public abstract SIDL.Boolean.Array1 getBoolArray(java.lang.String key,
//  						   SIDL.Boolean.Array1 dflt) 
//      throws gov.cca.TypeMismatchException;

  /**
   * Assign a key and value. Any value previously assigned
   * to the same key will be overwritten so long as it
   * is of the same type. If types conflict, an exception occurs.
   */
  public abstract void putInt(java.lang.String key,
			      int value) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  putLong[]
   */
  public abstract void putLong(java.lang.String key,
			       long value) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  putFloat[]
   */
  public abstract void putFloat(java.lang.String key,
				float value) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  putDouble[]
   */
  public abstract void putDouble(java.lang.String key,
				 double value) 
    throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putFcomplex[]
//     */
//    public abstract void putFcomplex(java.lang.String key,
//  				   SIDL.FloatComplex value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putDcomplex[]
//     */
//    public abstract void putDcomplex(java.lang.String key,
//  				   SIDL.DoubleComplex value) 
//      throws gov.cca.TypeMismatchException;

  /**
   * Method:  putString[]
   */
  public abstract void putString(java.lang.String key,
				 java.lang.String value) 
    throws gov.cca.TypeMismatchException;

  /**
   * Method:  putBool[]
   */
  public abstract void putBool(java.lang.String key,
			       boolean value) 
    throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putIntArray[]
//     */
//    public abstract void putIntArray(java.lang.String key,
//  				   SIDL.Integer.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putLongArray[]
//     */
//    public abstract void putLongArray(java.lang.String key,
//  				    SIDL.Long.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putFloatArray[]
//     */
//    public abstract void putFloatArray(java.lang.String key,
//  				     SIDL.Float.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putDoubleArray[]
//     */
//    public abstract void putDoubleArray(java.lang.String key,
//  				      SIDL.Double.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putFcomplexArray[]
//     */
//    public abstract void putFcomplexArray(java.lang.String key,
//  					SIDL.FloatComplex.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putDcomplexArray[]
//     */
//    public abstract void putDcomplexArray(java.lang.String key,
//  					SIDL.DoubleComplex.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putStringArray[]
//     */
//    public abstract void putStringArray(java.lang.String key,
//  				      SIDL.String.Array1 value) 
//      throws gov.cca.TypeMismatchException;

//    /**
//     * Method:  putBoolArray[]
//     */
//    public abstract void putBoolArray(java.lang.String key,
//  				    SIDL.Boolean.Array1 value) 
//      throws gov.cca.TypeMismatchException;

  /**
   * Make the key and associated value disappear from the object. 
   */
  public abstract void remove(java.lang.String key);

  /**
   *  Get all the names associated with the TypeMap
   *  without exposing the data implementation details.  The keys
   *  will be returned in an arbitrary order.
   *
   */
  public abstract String[] getAllKeys();

  /**
   * Return true if the key exists in this map 
   */
  public abstract boolean hasKey(java.lang.String key);

  /**
   * Return the type of the value associated with this key 
   */
  public abstract int typeOf(java.lang.String key);

  /**
   * Return the stringified form of the object associated with this key
   */
  public abstract String getAsString(java.lang.String key);
}
