/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.2 $ $Author: srikrish $ $Date: 2003/12/31 21:47:26 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * File:          ConnectionID.java
 * Symbol:        gov.cca.ConnectionID-v0.6
 * Symbol Type:   interface
 * Babel Version: 0.8.6
 * SIDL Created:  20031103 16:08:39 EST
 * Generated:     20031103 16:08:44 EST
 * Description:   Client-side glue code for gov.cca.ConnectionID
 * 
 */

package gov.cca;

/**
 * Symbol "gov.cca.ConnectionID" (version 0.6)
 * 
 *  This interface describes a CCA connection between components.
 *  A connection is made at the users direction
 *  when one component provides a Port that another component
 *  advertises for and uses.  The components are referred to by their
 *  opaque ComponentID references and the Ports are referred to
 *  by their string instance names.
 */
public interface ConnectionID {

  /**
   *  Get the providing component (callee) ID.
   *  @return ComponentID of the component that has 
   *          provided the Port for this connection. 
   *  @throws CCAException if the underlying connection 
   *            is no longer valid.
   */
  public abstract gov.cca.ComponentID getProvider() 
    throws gov.cca.CCAException;

  /**
   *  Get the using component (caller) ID.
   *  @return ComponentID of the component that is using the provided Port.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public abstract gov.cca.ComponentID getUser() 
    throws gov.cca.CCAException;

  /**
   *  Get the port name in the providing component of this connection.
   *  @return the instance name of the provided Port.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public abstract java.lang.String getProviderPortName() 
    throws gov.cca.CCAException;

  /**
   *  Get the port name in the using component of this connection.
   *  Return the instance name of the Port registered for use in 
   *  this connection.
   *  @throws CCAException if the underlying connection is no longer valid.
   */
  public abstract java.lang.String getUserPortName() 
    throws gov.cca.CCAException;
}
