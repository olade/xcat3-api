/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.3 $ $Author: srikrish $ $Date: 2004/01/02 01:10:35 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * File:          BuilderService.java
 * Symbol:        gov.cca.ports.BuilderService-v0.6
 * Symbol Type:   interface
 * Babel Version: 0.8.6
 * SIDL Created:  20031103 16:08:39 EST
 * Generated:     20031103 16:08:44 EST
 * Description:   Client-side glue code for gov.cca.ports.BuilderService
 * 
 */

package gov.cca.ports;

/**
 * Symbol "gov.cca.ports.BuilderService" (version 0.6)
 * 
 *    BuilderService is a Port implemented by a CCA compliant framework for
 * the purpose of composing components into applications in a standard way.
 * It is meant to expose the Component creation and composition functionality
 * without the specific framework implementation. This interface is expected 
 * to be useful for rapid application development in a scripting language. 
 * Other uses are generic application development environments for CCA 
 * applications. 
 * <p>Each of the fundamental component architecture pieces
 *    (instances of Component, Port, and Connection) may have
 *    an associated TypeMap of properties managed by the framework.
 *    The standardized keys in the properties of a Port are documented
 *    in Services.getPortProperties().
 *    The standardized keys in the properties of a Component and Connection
 *    are documented below.
 *  </p>
 *  <p>For connection, thus far:
 *    <pre>
 *    Key         value           meaning
 *    cca.isInUse boolean         true if there have been more successful
 * 				 getPort than releasePort calls for the
 * 				 connection at the the time 
 * 				 properties were fetched.
 *   </pre>
 *   </p>
 *  <P>For component, thus far:
 *   <pre>
 *    Key                 value           meaning
 *    cca.className       string          component type
 *   </pre>
 *  </p>
 */
public interface BuilderService 
  extends gov.cca.Port {

  /**
   * 	Creates an instance of a CCA component of the type defined by the 
   * 	string className.  The string classname uniquely defines the
   * 	"type" of the component, e.g.
   * 	    doe.cca.Library.GaussianElmination. 
   * 	It has an instance name given by the string instanceName.
   * 	The instanceName may be empty (zero length) in which case
   * 	the instanceName will be assigned to the component automatically.
   * 	@throws CCAException If the Component className is unknown, or if the
   * 		instanceName has already been used, a CCAException is thrown.
   * 	@return A ComponentID corresponding to the created component. Destroying
   * 		the returned ID does not destroy the component; 
   * 		see destroyInstance instead.
   */
  public abstract gov.cca.ComponentID createInstance(java.lang.String instanceName,
						     java.lang.String className,
						     gov.cca.TypeMap properties) 
    throws gov.cca.CCAException;

  /**
   *  Get component list.
   *  @return a ComponentID for each component currently created.
   */
  public abstract java.util.Collection getComponentIDs() 
    throws gov.cca.CCAException;

  /**
   *  Get property map for component.
   *  @return the public properties associated with the component referred to by
   *  ComponentID. 
   *  @throws a CCAException if the ComponentID is invalid.
   */
  public abstract gov.cca.TypeMap getComponentProperties(gov.cca.ComponentID cid) 
    throws gov.cca.CCAException;

  /**
   * 	Causes the framework implementation to associate the given properties 
   * 	with the component designated by cid. 
   * 	@throws CCAException if cid is invalid or if there is an attempted
   * 	change to a property locked by the framework implementation.
   */
  public abstract void setComponentProperties(gov.cca.ComponentID cid,
					      gov.cca.TypeMap map) 
    throws gov.cca.CCAException;

  /**
   * Get component id from stringified reference.
   *    @return a ComponentID from the string produced by 
   * 	ComponentID.getSerialization(). 
   *    @throws CCAException if the string does not represent the appropriate 
   * 	 serialization of a ComponentID for the underlying framework.
   */
  public abstract gov.cca.ComponentID getDeserialization(java.lang.String s) 
    throws gov.cca.CCAException;

  /**
   * Get id from name by which it was created.
   *  @return a ComponentID from the instance name of the component
   *  produced by ComponentID.getInstanceName().
   *  @throws CCAException if there is no component matching the 
   *  given componentInstanceName.
   */
  public abstract gov.cca.ComponentID getComponentID(java.lang.String componentInstanceName)
    throws gov.cca.CCAException;

  /**
   *  Eliminate the Component instance, from the scope of the framework.
   *  @param toDie the component to be removed.
   *  @param timeout the allowable wait; 0 means up to the framework.
   *  @throws CCAException if toDie refers to an invalid component, or
   *  if the operation takes longer than timeout seconds.
   */
  public abstract void destroyInstance(gov.cca.ComponentID toDie,
				       float timeout) 
    throws gov.cca.CCAException;

  /**
   *  Get the names of Port instances provided by the identified component.
   *  @param cid the component.
   *  @throws CCAException if cid refers to an invalid component.
   */
//    public abstract SIDL.String.Array1 getProvidedPortNames(gov.cca.ComponentID cid) 
//      throws gov.cca.CCAException;
  public abstract String[] getProvidedPortNames(gov.cca.ComponentID cid) 
    throws gov.cca.CCAException;

  /**
   *  Get the names of Port instances used by the identified component.
   *  @param cid the component.
   *  @throws CCAException if cid refers to an invalid component. 
   */
//    public abstract SIDL.String.Array1 getUsedPortNames(gov.cca.ComponentID cid) 
//      throws gov.cca.CCAException;
  public abstract String[] getUsedPortNames(gov.cca.ComponentID cid) 
    throws gov.cca.CCAException;

  /**
   *  Fetch map of Port properties exposed by the framework.
   *  @return the public properties pertaining to the Port instance 
   *    portname on the component referred to by cid. 
   *  @throws CCAException when any one of the following conditions occur:<ul>
   *    <li>portname is not a registered Port on the component indicated by cid,
   *    <li>cid refers to an invalid component. </ul>
   */
  public abstract gov.cca.TypeMap getPortProperties(gov.cca.ComponentID cid,
						    java.lang.String portName) 
    throws gov.cca.CCAException;

  /**
   *  Associates the properties given in map with the Port indicated by 
   *  portname. The component must have a Port known by portname.
   *  @throws CCAException if either cid or portname are
   * 	invalid, or if this a changed property is locked by 
   * 	 the underlying framework or component.
   */
  public abstract void setPortProperties(gov.cca.ComponentID cid,
					 java.lang.String portName,
					 gov.cca.TypeMap map) 
    throws gov.cca.CCAException;

  /**
   *   Creates a connection between ports on component user and 
   *   component provider. Destroying the ConnectionID does not
   *   cause a disconnection; for that, see disconnect().
   *   @throws CCAException when any one of the following conditions occur:<ul>
   *   <li>If either user or provider refer to an invalid component,
   *   <li>If either usingPortName or providingPortName refer to a 
   * 	 nonexistent Port on their respective component,
   *   <li>If other-- In reality there are a lot of things that can go wrong 
   * 	 with this operation, especially if the underlying connections 
   * 	 involve networking.</ul>
   */
  public abstract gov.cca.ConnectionID connect(gov.cca.ComponentID user,
					       java.lang.String usingPortName,
					       gov.cca.ComponentID provider,
					       java.lang.String providingPortName) 
    throws gov.cca.CCAException;

  /**
   * Returns a list of connections as an array of 
   * 	handles. This will return all connections involving components 
   * 	in the given componentList of ComponentIDs. This
   * 	means that ConnectionID's will be returned even if only one 
   * 	of the participants in the connection appears in componentList.
   * 
   * 	@throws CCAException if any component in componentList is invalid.
   */
  public abstract java.lang.Object getConnectionIDs(java.lang.Object componentList) 
    throws gov.cca.CCAException;

  /**
   *   Fetch property map of a connection.
   *   @return the properties for the given connection.
   *   @throws CCAException if connID is invalid.
   */
  public abstract gov.cca.TypeMap getConnectionProperties(gov.cca.ConnectionID connID)
    throws gov.cca.CCAException;

  /**
   * Associates the properties with the connection.
   *   @param map the source of the properties.
   *   @param connID connection to receive property values.
   *   @throws CCAException if connID is invalid, or if this changes 
   * 	 a property locked by the underlying framework.
   */
  public abstract void setConnectionProperties(gov.cca.ConnectionID connID,
					       gov.cca.TypeMap map) 
    throws gov.cca.CCAException;

  /**
   * Disconnect the connection indicated by connID before the indicated
   *     timeout in secs. Upon successful completion, connID and the connection
   *     it represents become invalid. 
   *     @param timeout the time in seconds to wait for a connection to close; 0
   *     means to use the framework implementation default.
   *     @throws CCAException when any one of the following conditions occur: <ul>
   *     <li>id refers to an invalid ConnectionID,
   *     <li>timeout is exceeded, after which, if id was valid before 
   * disconnect() was invoked, it remains valid
   * </ul>
   * 
   */
  public abstract void disconnect(gov.cca.ConnectionID connID,
				  float timeout) 
    throws gov.cca.CCAException;

  /**
   * Remove all connections between components id1 and id2 within 
   *   the period of timeout secs. If id2 is null, then all connections 
   *   to id1 are removed (within the period of timeout secs).
   *   @throws CCAException when any one of the following conditions occur:<ul>
   * 	  <li>id1 or id2 refer to an invalid ComponentID (other than id2 == null),
   * 	  <li>The timeout period is exceeded before the disconnections can be made. 
   * 	  </ul>
   */
  public abstract void disconnectAll(gov.cca.ComponentID id1,
				     gov.cca.ComponentID id2,
				     float timeout) 
    throws gov.cca.CCAException;
}
