/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.2 $ $Author: srikrish $ $Date: 2003/12/31 21:47:28 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * File:          ConnectionEvent.java
 * Symbol:        gov.cca.ports.ConnectionEvent-v0.6
 * Symbol Type:   interface
 * Babel Version: 0.8.6
 * SIDL Created:  20031103 16:08:39 EST
 * Generated:     20031103 16:08:42 EST
 * Description:   Client-side glue code for gov.cca.ports.ConnectionEvent
 * 
 */

package gov.cca.ports;

/**
 * Symbol "gov.cca.ports.ConnectionEvent" (version 0.6)
 * 
 * Event created when two components are connected.
 */
public interface ConnectionEvent {

  /**
   * <p>Returns the integer from those enumerated that describes the event.</p>
   * 
   * <p>
   * The semantics are noted before
   * each member of the enum/static constant. We can add in different
   * types of connect/disconnect as multiports and
   * explicit local/global/sync/async semantics are agreed to in the future.
   * At present we assume that:
   * <ul>
   * <li> All instances in a component cohort (often thought of as a single
   *   "parallel component") receive all the events
   *   and in the same order, but not necessarily globally synchronously.
   * 
   * <li> For disconnections, within a process the events are delivered first
   *   to the using component then (if necessary) to the providing
   *   component.
   * 
   * <li> For connections, within a process the events are delivered first
   *   to the providing component then (if necessary) to the using
   *   component.
   * </ul>
   * </p>
   * 
   * <p>
   * Clearly some of the assumptions above may not suit a component
   * instance in which multiple execution threads act on a
   * single instance of the <code>cca.Services</code> object (SMP). The Services
   * specification is ambiguous as to whether such a component is even
   * allowed.
   * </p>
   * <p>
   * When this is clarified, additional members of the enum may arise,
   * in which case the assumptions here apply only to
   * <code>ConnectPending</code>, <code>Connected</code>, <code>DisconnectPending</code>, 
   * <code>Disconnected</code> types.
   */
  public abstract int getEventType();

  /**
   * Get Properties of the affected Port.
   * Among the standard properties are the name and type info.
   */
  public abstract gov.cca.TypeMap getPortInfo();
}
