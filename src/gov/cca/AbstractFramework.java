/*
 * Indiana University Extreme! Lab Software License, Version 1.2
 *
 * Copyright (C) 2002 The Trustees of Indiana University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1) All redistributions of source code must retain the above
 *    copyright notice, the list of authors in the original source
 *    code, this list of conditions and the disclaimer listed in this
 *    license;
 *
 * 2) All redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the disclaimer
 *    listed in this license in the documentation and/or other
 *    materials provided with the distribution;
 *
 * 3) Any documentation included with all redistributions must include
 *    the following acknowledgement:
 *
 *      "This product includes software developed by the Indiana
 *      University Extreme! Lab.  For further information please visit
 *      http://www.extreme.indiana.edu/"
 *
 *    Alternatively, this acknowledgment may appear in the software
 *    itself, and wherever such third-party acknowledgments normally
 *    appear.
 *
 * 4) The name "Indiana Univeristy" or "Indiana Univeristy
 *    Extreme! Lab" shall not be used to endorse or promote
 *    products derived from this software without prior written
 *    permission from Indiana University.  For written permission,
 *    please contact http://www.extreme.indiana.edu/.
 *
 * 5) Products derived from this software may not use "Indiana
 *    Univeristy" name nor may "Indiana Univeristy" appear in their name,
 *    without prior written permission of the Indiana University.
 *
 * Indiana University provides no reassurances that the source code
 * provided does not infringe the patent or any other intellectual
 * property rights of any other entity.  Indiana University disclaims any
 * liability to any recipient for claims brought by any other entity
 * based on infringement of intellectual property rights or otherwise.
 *
 * LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH
 * NO WARRANTIES AS TO CAPABILITIES OR ACCURACY ARE MADE. INDIANA
 * UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 * SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR
 * OTHER PROPRIETARY RIGHTS.  INDIANA UNIVERSITY MAKES NO WARRANTIES THAT
 * SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN HORSES", "TRAP
 * DOORS", "WORMS", OR OTHER HARMFUL CODE.  LICENSEE ASSUMES THE ENTIRE
 * RISK AS TO THE PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS,
 * AND TO THE PERFORMANCE AND VALIDITY OF INFORMATION GENERATED USING
 * SOFTWARE.
 */

/**
 * @version $Revision: 1.2 $ $Author: srikrish $ $Date: 2003/12/31 21:47:25 $ (GMT) 
 * @author Sriram Krishnan [mailto:srikrish@extreme.indiana.edu]
 */

/*
 * File:          AbstractFramework.java
 * Symbol:        gov.cca.AbstractFramework-v0.6
 * Symbol Type:   interface
 * Babel Version: 0.8.6
 * SIDL Created:  20031103 16:08:39 EST
 * Generated:     20031103 16:08:44 EST
 * Description:   Client-side glue code for gov.cca.AbstractFramework
 * 
 */

package gov.cca;

/**
 * Symbol "gov.cca.AbstractFramework" (version 0.6)
 * 
 *  This is an interface presented by a CCA-compliant framework to access its 
 *  application framing capabilities. Most of the manipulation of the 
 *  underlying framework is expected to be be done with the 
 *  gov.cca.BuilderService Port. This class exists as a sort of bootstrap 
 *  to get a Services object necessary to retrieve Port's, including 
 *  BuilderService, from the underlying framework. How the interface and 
 *  the underlying framework is created is entirely unspecified and is up 
 *  to the devices of the programmer and the framework provider.
 * 
 *  <p>Example</p>
 *  <p>
 *  Here it is assumed that an instance of AbstractFramework
 *  is created in the main() from some hypothetical implementation.
 *  The idea is to allow a complete swap of framework choice by 
 *  changing out the specified implementation class of a framework.
 *  </p>
 * 
 *  <code><pre>
 *  // java
 *  main() {
 *    cca.reference.Framework fwkimpl = new cca.reference.Framework();
 *    // change fwkimpl above to use different cca implementations when
 *    // AbstractFramework becomes part of the standard.
 *    gov.cca.AbstractFramework fwk = (gov.cca.AbstractFramework)fwkimpl;
 *    gov.cca.Services svc = 
 * 	   fwk.getServices("instance0","AppDriver",null);
 *    // From here on, access all services, components, etc
 *    // through svc.
 *    ...
 *    // when done
 *    fwk.releaseServices(svc);
 *    fwk.shutdownFramework();
 *  }
 * 
 *  // c++
 *  int functionName() {
 *    ::gov::sandia::ccafe::Framework fwkimpl;
 *    ::gov::cca::AbstractFrameworkPtr fwk;
 * 
 *    fwk = fwkimpl.getStandardFramework();
 *    ::gov::cca::Services_Interface * svc = 0;
 *    svc = fwk->getServices("instance0","AppDriver",0);
 *    // From here on, access all services, components, etc
 *    // through svc.
 *    ...
 *    // when done
 *    fwk->releaseServices(svc);
 *    svc = 0;
 *    fwk->shutdownFramework();
 * 
 *    // at scope exit, all memory is automatically cleaned up.
 *  }
 *  </pre></code>
 */
public interface AbstractFramework {

  /**
   *  Create an empty TypeMap. Presumably this would be used in 
   *  an ensuing call to <code>getServices()</code>. The "normal" method of
   *  creating typemaps is found in the <code>Services</code> interface. It
   *  is duplicated here to break the "chicken and egg" problem.
   */
  public abstract gov.cca.TypeMap createTypeMap() 
    throws gov.cca.CCAException;

  /**
   * Retrieve a Services handle to the underlying framework. 
   * This interface effectively causes the calling program to 
   * appear as the image of a component inside the framework.
   * This method may be called any number of times
   * with different arguments, creating a new component image 
   * each time. 
   * The only proper method to destroy a Services obtained 
   * from this interface is to pass it to releaseServices.
   * 
   * @param selfInstanceName the Component instance name,
   * as it will appear in the framework.
   * 
   * @param selfClassName the Component type of the 
   * calling program, as it will appear in the framework. 
   * 
   * @param selfProperties (which can be null) the properties 
   * of the component image to appear. 
   * 
   * @throws CCAException in the event that selfInstanceName 
   * is already in use by another component.
   * 
   * @return  A Services object that pertains to the
   * 	    image of the this component. This is identical
   * 	    to the object passed into Component.setServices() 
   * 	    when a component is created.
   */
  public abstract gov.cca.Services getServices(java.lang.String selfInstanceName,
					       java.lang.String selfClassName,
					       gov.cca.TypeMap selfProperties) 
    throws gov.cca.CCAException;

  /**
   * Inform framework that the <code>Services</code> handle is no longer needed by the 
   * caller and that the reference to its component image is to be
   * deleted from the context of the underlying framework. This invalidates
   * any <code>ComponentID</code>'s or <code>ConnectionID</code>'s associated 
   * with the given <code>Services</code>' component image. 
   * 
   * @param svc The result of getServices earlier obtained.
   * 
   * @throws CCAException if the <code>Services</code>
   *         handle has already been released or is otherwise rendered invalid 
   *         or was not obtained from <code>getServices()</code>.
   */
  public abstract void releaseServices(gov.cca.Services svc) 
    throws gov.cca.CCAException;

  /**
   * Tell the framework it is no longer needed and to clean up after itself. 
   *  @throws CCAException if the framework has already been shutdown.
   */
  public abstract void shutdownFramework() 
    throws gov.cca.CCAException;

  /**
   * Creates a new framework instance based on the same underlying 
   * framework implementation. This does not copy the existing 
   * framework, nor are any of the user-instantiated components in
   * the original framework available in the newly created 
   * <code>AbstractFramework</code>. 
   * 
   * @throws CCAException when one of the following conditions occur:
   * 
   * (1)the AbstractFramework previously had shutdownFramework() called on it, or 
   * (2)the underlying framework implementation does not permit creation 
   * of another instance.	 
   */
  public abstract gov.cca.AbstractFramework createEmptyFramework() 
    throws gov.cca.CCAException;
}
